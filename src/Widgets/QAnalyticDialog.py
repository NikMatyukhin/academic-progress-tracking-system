import sys
sys.path.insert(0, "D:\\QtProjects\\apts\\Project\\src")

from PySide2.QtGui import QPainter
from PySide2.QtCharts import QtCharts
from PySide2.QtCore import Qt
from PySide2.QtWidgets import QDialog, QApplication

from Forms.ui_dialog import Ui_Dialog
from Database.LocalService import LocalService
from Analytic.MarksAnalyzer import MarksAnalyzer

class QAnalyticDialog(QDialog):
    def __init__(self, parent=None):
        QDialog.__init__(self, parent)
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.setWindowTitle("Статистика сдачи работ")

        self.service = LocalService()
        self.engine = MarksAnalyzer()

        self.groupChart = QtCharts.QChartView()
        self.ui.verticalLayout.addWidget(self.groupChart)

        self.chart = QtCharts.QChart()

        self.categories = [f"Работа {i}" for i in range(1,6)]
        self.axisX = QtCharts.QBarCategoryAxis()
        self.axisX.append(self.categories)

        self.axisY = QtCharts.QValueAxis()
        self.axisY.setRange(0, 700)

        self.groupList = ["Все группы"] + list(map(lambda x: f"КИ{x[1]%100}-{x[2]}/{x[3]}Б",
                                                   self.service.getList("groups", "")))
        self.ui.comboBox.insertItems(0, self.groupList)
        self.group = self.groupList[0]
        self.comboBoxGroupChanged()
        self.ui.pushButton.clicked.connect(self.comboBoxGroupChanged)

    @property
    def data(self):
        self.with_group = False
        return self.service.getMarksTable(self.ui.comboBox.currentText(), self.with_group, 6)

    def currentGroupChanged(self, text):
        self.group = text

    def comboBoxGroupChanged(self):
        max = 0
        self.group = self.ui.comboBox.currentText()
        set = QtCharts.QBarSet("Оценка")
        self.series = QtCharts.QBarSeries()

        data = []
        self.with_group = self.group in ['Все группы']

        table = self.service.getMarksTable(self.group, self.with_group, 6)
        for line in table:
            pack = {'score': []}
            if self.with_group:
                pack['group'] = f'КИ{line[3]%100}-{line[4]}/{line[5]}'
            for i in range(6):
                score = line[3 + self.with_group * 3 + i * 3]
                if score is None:
                    score = 0
                deadline = MarksAnalyzer.getDeadlineScore(line[4 + self.with_group * 3 + i * 3], line[5 + self.with_group * 3 + i * 3])
                if deadline is None:
                    deadline = 0
                pack['score'].append(score)
                pack['score'].append(deadline)
            mn = MarksAnalyzer.scoreMean(pack['score'])
            pack['mean'] = mn if mn else 0
            data.append(pack)
        for score, value in self.engine.assessmentsFrequency(data).items():
            set.append(value)
            if value > max:
                max = value
                self.axisY.setRange(0, max)
        self.series.append(set)

        self.chart = QtCharts.QChart()

        self.chart.addSeries(self.series)
        self.chart.setTitle('Насколько хорошо учатся студенты')
        self.chart.setAnimationOptions(QtCharts.QChart.SeriesAnimations)
        self.chart.addAxis(self.axisX, Qt.AlignBottom)
        self.series.attachAxis(self.axisX)
        self.chart.addAxis(self.axisY, Qt.AlignLeft)
        self.series.attachAxis(self.axisY)

        self.groupChart.setChart(self.chart)
        self.groupChart.setRenderHint(QPainter.Antialiasing)