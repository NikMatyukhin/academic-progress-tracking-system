from PySide2.QtCore import (Qt, QRect, QPoint)
from PySide2.QtWidgets import (QHeaderView)
from PySide2.QtGui import (QColor, QPainter, QStandardItemModel, QStandardItem, QBrush, QPen, QTextOption)

SCORE_COLUMN_WIDTH = 75
SUB_SECTION_HEIGHT = 25

class QHierarchicalHeaderView(QHeaderView):
    def __init__(self, works_number: int, with_group: bool, with_mean: bool, orientation: Qt.Orientation):
        QHeaderView.__init__(self, orientation)

        self.column = 1 + works_number * 2 + with_group + with_mean
        self.with_group = with_group
        self.with_mean = with_mean

        model = QStandardItemModel(1, self.column)
        model.setItem(0, 0, QStandardItem("Имя, фамилия"))

        if with_group:
            model.setItem(0, 1, QStandardItem("Группа"))

        for i in range(1 + with_group, works_number * 2 + self.with_group + self.with_mean, 2):
            model.setItem(0, i, QStandardItem("Оценка"))
            model.setItem(0, i + 1, QStandardItem("Дедлайн"))

        if with_mean:
            model.setItem(0, self.column - 1, QStandardItem("Средний балл"))

        self.setModel(model)

    def paintSection(self, painter: QPainter, rect: QRect, logicalIndex: int):
        rect.setTopLeft(QPoint(rect.left() - 1, rect.top() - 1))
        rect.setBottomRight(QPoint(rect.right() - 1, rect.bottom() - 1))

        end = self.column - self.with_mean

        opt = QTextOption()
        opt.setAlignment(Qt.AlignCenter)

        brushGray = QBrush(QColor(225, 225, 225), Qt.SolidPattern)
        penGray = QPen(QColor(155, 155, 155))
        penBlack = QPen(QColor(0, 0, 0))

        mainSectionName = f"Практическая работа {(logicalIndex + (not self.with_group)) // 2}"

        painter.save()

        if end > logicalIndex > (0 + self.with_group):
            painter.setPen(penGray)
            painter.setBrush(brushGray)
            if not (logicalIndex + self.with_group) % 2:
                rect.setLeft(rect.left() - SCORE_COLUMN_WIDTH)
            rect.setWidth(SCORE_COLUMN_WIDTH * 2)
            rect.setHeight(SUB_SECTION_HEIGHT)
            painter.drawRect(rect)
            painter.setPen(penBlack)
            painter.drawText(rect, mainSectionName, opt)

            rect.setTop(SUB_SECTION_HEIGHT - 1)
            rect.setHeight(SUB_SECTION_HEIGHT + 2)
            if not (logicalIndex + self.with_group) % 2:
                rect.setLeft(rect.left() + SCORE_COLUMN_WIDTH)
            rect.setWidth(SCORE_COLUMN_WIDTH)
            painter.setPen(QColor(155,155,155))
            painter.setBrush(QBrush(QColor(225,225,225), Qt.SolidPattern))
            painter.drawRect(rect)
            painter.setPen(Qt.black)
            painter.drawText(rect, self.model().item(0, logicalIndex).text(), opt)
        else:
            painter.setPen(penGray)
            painter.setBrush(brushGray)
            painter.drawRect(rect)
            painter.setPen(penBlack)
            painter.drawText(rect, self.model().item(0, logicalIndex).text(), opt)

        painter.restore()