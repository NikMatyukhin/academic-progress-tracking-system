from PySide2.QtCore import (Qt, QRect, QSize, Signal)
from PySide2.QtWidgets import (QPushButton, QStyle)
from PySide2.QtGui import (QPainter, QPen, QBrush, QColor)


class QColumnButton(QPushButton):
    activated = Signal(str)

    def __init__(self, text):
        QPushButton.__init__(self, text)
        self.m_size = QSize(100, 40)
        self.m_state = QStyle.State_MouseOver

    def isActive(self):
        if self.m_state & QStyle.State_Active:
            return True
        return False

    def activate(self):
        self.m_state = QStyle.State_Active

    def disactivate(self):
        self.m_state = QStyle.State_MouseOver
        self.update()

    def minimumSizeHint(self):
        return self.m_size

    def sizeHint(self):
        return self.m_size

    def mousePressEvent(self, event):
        self.pressed.emit()
        if self.m_state & QStyle.State_Active:
            event.ignore()
        else:
            self.m_state = QStyle.State_Sunken
            self.update()

    def mouseReleaseEvent(self, event):
        self.released.emit()
        self.clicked.emit()
        if self.m_state & QStyle.State_Active:
            event.ignore()
        else:
            self.m_state = QStyle.State_Active
            self.activated.emit(self.text())
        self.update()

    def enterEvent(self, event):
        if self.m_state & QStyle.State_Active:
            event.ignore()
        else:
            self.m_state = QStyle.State_HasFocus
            self.update()

    def leaveEvent(self, event):
        if self.m_state & QStyle.State_Active:
            event.ignore()
        else:
            self.m_state = QStyle.State_MouseOver
            self.update()

    def paint(self, painter, brush, drawAdditionalLine = False):
        painter.setPen(QPen(brush, 1, Qt.SolidLine))
        painter.setBrush(brush)
        painter.drawRect(QRect(0,0,100,40))
        painter.setPen(QColor(155,155,155))
        if drawAdditionalLine:
            painter.drawLine(0,0,100,0)
            painter.drawLine(0,39,101,40)
        else:
            painter.drawLine(100,0,100,40)
        painter.setPen(Qt.black)
        painter.drawText(self.rect(), Qt.AlignCenter, self.text())

    def paintEvent(self, event):
        painter = QPainter(self)
        if self.m_state & QStyle.State_MouseOver:
            self.paint(painter, QBrush(QColor(194,199,203), Qt.SolidPattern))
        elif self.m_state & QStyle.State_HasFocus:
            self.paint(painter, QBrush(QColor(194,183,203), Qt.SolidPattern))
        elif self.m_state & QStyle.State_Sunken:
            self.paint(painter, QBrush(QColor(194,190,203), Qt.SolidPattern))
        elif self.m_state & QStyle.State_Active:
            self.paint(painter, QBrush(QColor(240,240,240), Qt.SolidPattern), True)