from PySide2.QtWidgets import (QStyledItemDelegate, QComboBox,
                               QStyleOptionViewItem, QLineEdit)
from PySide2.QtCore import (Qt)


class QComboBoxDelegate(QStyledItemDelegate):
    def __init__(self):
        QStyledItemDelegate.__init__(self)
        self.listItems = ['Не сдал','1','2','3','4','5']

    def createEditor(self, parent, option: QStyleOptionViewItem, index):
        editor = QComboBox(parent)
        editor.setFrame(False)
        editor.addItems(self.listItems)
        value = index.model().data(index) if index.model().data(index) != None else 'Не сдал'
        editor.setCurrentIndex(self.listItems.index(str(value)))
        editor.setStyleSheet('''QComboBox { padding-left: 10px; }
                                QComboBox:drop-down { width: 0px; }
                                QComboBox QAbstractItemView { outline: 0px;
                                                              background-color: #F0F0F0;
                                                              selection-color: black;
                                                              selection-background-color: #C2C7CB; } ''')
        return editor

    def setEditorData(self, editor: QComboBox, index):
        value = index.model().data(index)
        editor.setCurrentText(str(value).center(7))

    def setModelData(self, editor: QComboBox, model, index):
        value = editor.currentText()
        if value == 'Не сдал':
            value = None
        model.setData(index, value)

    def updateEditorGeometry(self, editor: QComboBox, option, index):
        editor.setGeometry(option.rect)