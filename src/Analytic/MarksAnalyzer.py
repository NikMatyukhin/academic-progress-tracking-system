import math
import enum
import numpy
from datetime import datetime


class MarksAnalyzer:
    @staticmethod
    def checkMean(assessment_table: list):
        for line in assessment_table:
            if all(line):
                return True
        return False

    @staticmethod
    def scoreMean(assessment_list: list):
        assessment_list = numpy.reshape(numpy.array(assessment_list), -1)
        if all(assessment_list):
            return round(numpy.mean(assessment_list), 2)
        return None

    @staticmethod
    def getDeadlineScore(pass_date, deadline_date):
        if all([pass_date, deadline_date]):
            pass_date = datetime.strptime(pass_date, '%d.%m.%Y')
            deadline_date = datetime.strptime(deadline_date, '%d.%m.%Y')
            if pass_date < deadline_date:
                return 5
            else:
                diff = deadline_date - pass_date
                assessment = 5 - math.ceil(abs(diff.days) // 7)
                return assessment if assessment > 0 else 0
        else:
            return None

    @staticmethod
    def scoreСlassifier(assessment_list: list):
        Achievements = enum.Enum('Achievements', 'DEFAULT GOOD GREAT EXCELLENT INCREDIBLE IDEAL')
        assessment_list = numpy.reshape(numpy.array(assessment_list), -1)
        mean = round(numpy.mean(assessment_list), 2)
        if mean == 5.0:
            return Achievements.IDEAL.value
        elif all([not i in assessment_list for i in [0,1,2,3]]) and not mean < 4.2:
            if mean >= 4.8:
                return Achievements.INCREDIBLE.value
            elif mean >= 4.6:
                return Achievements.EXCELLENT.value
            elif mean >= 4.4:
                return Achievements.GREAT.value
            elif mean >= 4.2:
                return Achievements.GOOD.value
        return Achievements.DEFAULT.value

    @staticmethod
    def assessmentsFrequency(table: list):
        result = {1:0, 2:0, 3:0, 4:0, 5:0}
        for line in table:
            for score in range(1,6):
                result[score] += line['score'].count(score)
        return result