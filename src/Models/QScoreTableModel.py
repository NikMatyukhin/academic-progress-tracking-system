from PySide2.QtCore import (Qt, QAbstractTableModel, QModelIndex)


class QScoreTableModel(QAbstractTableModel):
    def __init__(self, works_number, with_group, with_mean, parent=None):
        QAbstractTableModel.__init__(self, parent)
        self.student_progress = []
        self.works_number = works_number
        self.with_group = with_group
        self.with_mean = with_mean
        self.column_names = ["name"]
        if with_group:
            self.column_names.append("group")
        for i in range(works_number):
            self.column_names.append(f"score{i}")
            self.column_names.append(f"deadline{i}")
        if with_mean:
            self.column_names.append("mean")

    def rowCount(self, index=QModelIndex()):
        return len(self.student_progress)

    def columnCount(self, index=QModelIndex()):
        return len(self.column_names)

    def data(self, index, role=Qt.DisplayRole):
        if index.isValid():
            if role == Qt.TextAlignmentRole:
                if index.column() > 0:
                    return Qt.AlignCenter
            if role == Qt.DisplayRole:
                if 0 <= index.row() < len(self.student_progress):
                    return self.student_progress[index.row()][self.column_names[index.column()]]
        return None

    def insertRows(self, position, rows=1, index=QModelIndex()):
        self.beginInsertRows(QModelIndex(), position, position + rows - 1)
        pack = {"name" : None}
        if self.with_group:
            pack["group"] = None
        for i in range(self.works_number):
            pack[f"score{i}"] = None
            pack[f"deadline{i}"] = None
        if self.with_mean:
            pack["mean"] = None
        for row in range(rows):
            self.student_progress.insert(position + row, pack)
        self.endInsertRows()
        return True

    def appendRow(self, line: dict):
        pos = self.rowCount()
        self.insertRows(pos)
        for key, item in line.items():
            self.setData(self.index(pos, self.column_names.index(key)), item)

    def removeRows(self, position, rows=1, index=QModelIndex()):
        self.beginRemoveRows(QModelIndex(), position, position + rows - 1)
        del self.student_progress[position:position+rows]
        self.endRemoveRows()
        return True

    def setData(self, index, value, role=Qt.EditRole):
        if role == Qt.EditRole:
            if index.isValid():
                if 0 <= index.row() < len(self.student_progress):
                    self.student_progress[index.row()][self.column_names[index.column()]] = value
                    self.dataChanged.emit(index, index, 0)
                    return True
        return False

    def flags(self, index):
        col = index.column() - int(self.with_group)
        if col > 0 and col % 2:
            if col != self.with_group + self.with_mean + self.works_number * 2 - 1:
                return Qt.ItemFlags(QAbstractTableModel.flags(self, index) | Qt.ItemIsEditable)
        return Qt.ItemFlags(QAbstractTableModel.flags(self, index))