# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'mainwindow.ui'
##
## Created by: Qt User Interface Compiler version 5.15.0
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import (QCoreApplication, QDate, QDateTime, QMetaObject,
    QObject, QPoint, QRect, QSize, QTime, QUrl, Qt)
from PySide2.QtGui import (QBrush, QColor, QConicalGradient, QCursor, QFont,
    QFontDatabase, QIcon, QKeySequence, QLinearGradient, QPalette, QPainter,
    QPixmap, QRadialGradient)
from PySide2.QtWidgets import *


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(900, 600)
        MainWindow.setMinimumSize(QSize(900, 600))
        MainWindow.setContextMenuPolicy(Qt.PreventContextMenu)
        MainWindow.setToolTipDuration(0)
        MainWindow.setLayoutDirection(Qt.LeftToRight)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.centralwidget.setLayoutDirection(Qt.LeftToRight)
        self.tabWidget = QTabWidget(self.centralwidget)
        self.tabWidget.setObjectName(u"tabWidget")
        self.tabWidget.setGeometry(QRect(100, 0, 800, 100))
        self.tabWidget.setMinimumSize(QSize(800, 100))
        self.tabWidget.setMaximumSize(QSize(16777215, 100))
        self.tabWidget.setCursor(QCursor(Qt.ArrowCursor))
        self.tabWidget.setIconSize(QSize(16, 16))
        self.tabWidget.setMovable(False)
        self.layoutWidget = QWidget(self.centralwidget)
        self.layoutWidget.setObjectName(u"layoutWidget")
        self.layoutWidget.setGeometry(QRect(0, 0, 905, 582))
        self.verticalLayout = QVBoxLayout(self.layoutWidget)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.syncronizeButton = QPushButton(self.layoutWidget)
        self.syncronizeButton.setObjectName(u"syncronizeButton")
        self.syncronizeButton.setMinimumSize(QSize(100, 100))
        self.syncronizeButton.setMaximumSize(QSize(100, 100))
        self.syncronizeButton.setCursor(QCursor(Qt.ArrowCursor))
        self.syncronizeButton.setContextMenuPolicy(Qt.NoContextMenu)
        self.syncronizeButton.setIconSize(QSize(80, 80))
        self.syncronizeButton.setFlat(False)

        self.horizontalLayout.addWidget(self.syncronizeButton)

        self.topMenu = QWidget(self.layoutWidget)
        self.topMenu.setObjectName(u"topMenu")
        self.topMenu.setMinimumSize(QSize(800, 100))
        self.topMenu.setMaximumSize(QSize(16777215, 100))

        self.horizontalLayout.addWidget(self.topMenu)


        self.verticalLayout.addLayout(self.horizontalLayout)

        self.horizontalLayout2 = QHBoxLayout()
        self.horizontalLayout2.setSpacing(0)
        self.horizontalLayout2.setObjectName(u"horizontalLayout2")
        self.workingPlace = QWidget(self.layoutWidget)
        self.workingPlace.setObjectName(u"workingPlace")
        self.workingPlace.setMinimumSize(QSize(800, 470))

        self.horizontalLayout2.addWidget(self.workingPlace)


        self.verticalLayout.addLayout(self.horizontalLayout2)

        self.buttonColumn = QWidget(self.centralwidget)
        self.buttonColumn.setObjectName(u"buttonColumn")
        self.buttonColumn.setGeometry(QRect(0, 100, 101, 476))
        self.buttonColumn.setMinimumSize(QSize(101, 470))
        self.buttonColumn.setMaximumSize(QSize(101, 16777215))
        self.buttonColumn.setContextMenuPolicy(Qt.PreventContextMenu)
        self.buttonColumn.setToolTipDuration(0)
        self.buttonColumn.setLayoutDirection(Qt.LeftToRight)
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QStatusBar(MainWindow)
        self.statusbar.setObjectName(u"statusbar")
        self.statusbar.setMinimumSize(QSize(0, 30))
        self.statusbar.setMaximumSize(QSize(16777215, 30))
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)

        self.tabWidget.setCurrentIndex(-1)


        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle("")
        self.syncronizeButton.setText("")
    # retranslateUi

