import sys

from Resources import rc_icons

from Forms.ui_mainwindow import Ui_MainWindow

from Widgets.QColumnButton import QColumnButton
from Widgets.QComboBoxDelegate import QComboBoxDelegate
from Widgets.QHierarchicalHeaderView import (QHierarchicalHeaderView, SCORE_COLUMN_WIDTH)
from Widgets.QAnalyticDialog import QAnalyticDialog

from Analytic.MarksAnalyzer import MarksAnalyzer

from Database.LocalService import LocalService

from Models.QScoreTableModel import QScoreTableModel

from PySide2.QtWidgets import (QApplication, QMainWindow, QPushButton, QHBoxLayout, QVBoxLayout, QTableView, QHeaderView, QAction, QToolBar)
from PySide2.QtGui import (QIcon)
from PySide2.QtCore import (Qt)


class APTS(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.local_service = LocalService()

        # Массив кнопок переключения между группами
        # Храню в массиве, так как отдельно обрабатываю логику их переключения
        self.buttons = []

        # Установка главного слоя в центральный виджет, потому что в дизайнере не получилось
        self.ui.centralwidget.setLayout(self.ui.verticalLayout)

        # Установка иконки кнопки синхронизации баз данных
        self.ui.syncronizeButton.setIcon(QIcon(":/database.png"))

        # Заполнение ряда для вкладки "Файл"
        self.newBase = QAction(QIcon(":/file.png"), "Новая база")
        self.newBase.triggered.connect(self.slot1)
        self.newBase.setDisabled(True)
        self.delBase = QAction(QIcon(":/destroied-file.png"), "Удалить базу")
        self.delBase.triggered.connect(self.slot1)
        self.delBase.setDisabled(True)
        self.saveToCSV = QAction(QIcon(":/csv.png"), "Сохранить\nв CSV")
        self.saveToCSV.triggered.connect(self.slot1)
        self.fillPaneOfTabWidget("Файл", self.newBase, self.delBase, self.saveToCSV)

        # Заполнение ряда для вкладки "Поток"
        self.newCourse = QAction(QIcon(":/person.png"), "Новый\nпоток")
        self.newCourse.triggered.connect(self.slot1)
        self.groups = QAction(QIcon(":/teamwork.png"), "Список\nгрупп")
        self.groups.triggered.connect(self.slot1)
        self.tasks = QAction(QIcon(":/tasks.png"), "Список\nработ")
        self.tasks.triggered.connect(self.slot1)
        self.fillPaneOfTabWidget("Поток", self.newCourse, self.groups, self.tasks)

        # Заполнение ряда для вкладки "Студенты"
        self.newStudent = QAction(QIcon(":/student.png"), "Новый\nстудент")
        self.newStudent.triggered.connect(self.slot1)
        self.personal = QAction(QIcon(":/control.png"), "Подробная\nинформация")
        self.personal.triggered.connect(self.slot1)
        self.fillPaneOfTabWidget("Студенты", self.newStudent, self.personal)

        # Заполнение ряда для вкладки "Практические"
        self.newTask = QAction(QIcon(":/task.png"), "Новое\nзадание")
        self.newTask.triggered.connect(self.slot1)
        self.deadlines = QAction(QIcon(":/calendar.png"), "Дедлайны")
        self.deadlines.triggered.connect(self.slot1)
        self.fillPaneOfTabWidget("Практические", self.newTask, self.deadlines)

        # Заполнение ряда для вкладки "Достижения"
        self.newAchievement = QAction(QIcon(":/trophy.png"), "Новое\nдостижение")
        self.newAchievement.triggered.connect(self.slot1)
        self.fillPaneOfTabWidget("Достижения", self.newAchievement)

        # Заполнение ряда для вкладки "Справка"
        self.reference = QAction(QIcon(":/files-and-folders.png"), "Открыть\nсправку")
        self.reference.triggered.connect(self.slot1)
        self.fillPaneOfTabWidget("Справка", self.reference)

        # Заполнение ряда для вкладки "Статистика"
        self.statistic = QAction(QIcon(":/statistic.png"), "Открыть\nстатистику")
        self.statistic.triggered.connect(self.analyticWindowOpened)
        self.fillPaneOfTabWidget("Статистика", self.statistic)

        # Установка правого углового виджета у вкладок с инструментами
        # В качестве углового виджета выступает кнопка редиректа на репозиторий проекта
        self.repositoryButton = QPushButton("Gitlab")
        self.repositoryButton.setObjectName("repositoryButton")
        self.ui.tabWidget.setCornerWidget(self.repositoryButton, Qt.TopRightCorner)

        # Верхний слой с вкладками инструментов
        topLayout = QHBoxLayout()
        topLayout.addWidget(self.ui.tabWidget)
        topLayout.setMargin(0)
        self.ui.topMenu.setLayout(topLayout)

        # Заполнение левой колонки кнопок переключения между группами
        self.fillButtonsColumn(list(map(lambda x: f"КИ{x[1]%100}-{x[2]}/{x[3]}Б",
                                                  self.local_service.getList("groups", ""))))

        # Важные переменные, ответственные за отображение колонок "группа" и "средний балл", а также количество практических
        self.with_group = True
        self.with_mean = False
        self.works_number = len(self.local_service.getList("works", ""))

        # Центральный слой с табличным представлением для оценок
        # Заключено в прокручиваемую область, так как таблица безразмерна как горизонтально, так и вертикально
        self.centerLayout = QHBoxLayout()
        self.view = None

        # загружаем начальную таблицу "Все группы"
        self.updateModel("Все группы")

        self.centerLayout.setMargin(20)

        self.ui.workingPlace.setLayout(self.centerLayout)

        #Название и иконка главного окна
        self.setWindowTitle("Система отслеживания академической успеваемости")
        self.setWindowIcon(QIcon(":/window-icon.png"))

    def slot1(self):
        pass

    def analyticWindowOpened(self):
        win = QAnalyticDialog(self)
        win.exec_()

    def fillPaneOfTabWidget(self, paneName: str, *actions: QAction):
        toolBar = QToolBar()
        for act in actions:
            font = act.font()
            font.setPointSize(8)
            act.setFont(font)
            toolBar.addAction(act)
            toolBar.addSeparator()
        toolBar.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.ui.tabWidget.addTab(toolBar, paneName)

    def fillButtonsColumn(self, nameList: str):
        leftColumn = QVBoxLayout()
        nameList.insert(0, "Все группы")
        nameList.append("Недопуск")
        # Проходимся по списку "Все группы" ... "Недопуск"
        for name in nameList:
            # Создаем кнопку с соответствующим названием
            button = QColumnButton(name)
            # Соединяем со слотом переключения
            button.activated.connect(self.activateButton)
            # Сохраняем в массиве кнопок переключения
            self.buttons.append(button)
            # Собираем в колонну
            leftColumn.addWidget(button)
        # Активной делаем первую кнопку (переключение на "Все группы")
        self.buttons[0].activate()
        leftColumn.addWidget(self.ui.buttonColumn)
        leftColumn.setMargin(0)
        self.ui.horizontalLayout2.insertLayout(0, leftColumn)

    def activateButton(self, name: str):
        self.with_group = name in ['Все группы','Недопуск']
        for button in self.buttons:
            # Ищем активную кнопку с именем, отличным от имени текущей активной кнопки
            if button.text() != name and button.isActive():
                # Деактивируем предыдущую активную кнопку
                button.disactivate()
                break
        # Вызываем обновление модели
        self.updateModel(name)

    def updateModel(self, name):
        table = self.local_service.getMarksTable(name, self.with_group, self.works_number)

        self.with_mean = MarksAnalyzer.checkMean(table)

        if not self.centerLayout.isEmpty():
            self.centerLayout.removeWidget(self.view)

        self.view = QTableView()

        header = QHierarchicalHeaderView(self.works_number, self.with_group, self.with_mean, Qt.Horizontal)
        header.setSectionResizeMode(QHeaderView.Fixed)

        model = QScoreTableModel(self.works_number, self.with_group, self.with_mean)
        pack = dict()
        for line in table:
            pack['name'] = ' '.join([line[0], line[1], line[2]])
            if self.with_group:
                pack['group'] = f'КИ{line[3]%100}-{line[4]}/{line[5]}'
            for i in range(self.works_number):
                pack[f'score{i}'] = line[3 + self.with_group * 3 + i * 3]
                pack[f'deadline{i}'] = MarksAnalyzer.getDeadlineScore(line[4 + self.with_group * 3 + i * 3],
                                                                      line[5 + self.with_group * 3 + i * 3])
            if self.with_mean:
                mn = MarksAnalyzer.scoreMean([(pack[f'score{i}'], pack[f'deadline{i}']) for i in range(self.works_number)])
                pack['mean'] = str(mn) if mn else ''
            model.appendRow(pack)

        # Устанавливаем модель, делаем оба скролл бара всегда активными, скрываем вертикальный заголовок
        self.view.setModel(model)
        self.view.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.view.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.view.verticalHeader().hide()
        self.view.setHorizontalHeader(header)

        # Фиксируем ширины колонок
        self.view.setColumnWidth(0, 250)
        viewDelegate = QComboBoxDelegate()
        if self.with_group:
            self.view.setColumnWidth(1, 90)
        for i in range(1+self.with_group, self.works_number * 2 + 1 + self.with_group):
            self.view.setColumnWidth(i, SCORE_COLUMN_WIDTH)
            if (i - self.with_group) % 2:
                self.view.setItemDelegateForColumn(i, viewDelegate)
        if self.with_mean:
            self.view.setColumnWidth(self.works_number * 2 + 1 + self.with_group, 100)

        self.centerLayout.addWidget(self.view)


if __name__ == "__main__":
    app = QApplication([])
    app.setStyleSheet(open("Resources/stylesheet.qss", "r").read())
    window = APTS()
    window.show()
    sys.exit(app.exec_())
