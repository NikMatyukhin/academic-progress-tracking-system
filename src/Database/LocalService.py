import sqlite3

from .DatabaseWrapperInterface import DatabaseWrapperInterface


class LocalService(DatabaseWrapperInterface):
    def __init__(self):
        self.conn = sqlite3.connect("apts.db")
        self.cursor = self.conn.cursor()

    def getID(self, table: str, **fields: str) -> int:
        sq = f"select id from {table} where "
        for field in fields:
            if not sq.endswith(" where "):
                sq += "and "
            sq += f"{field}=:{field} "
        self.cursor.execute(sq, fields)
        return self.cursor.fetchone()[0]

    def getName(self, table: str, ID: int) -> str:
        sq = None
        if table == "students":
            sq = "select first_name, middle_name, second_name from students where id = :id"
        elif table == "groups":
            sq = "select year, number, subgroup from groups where id = :id"
        elif table == "works":
            sq = "select name from works where id = :id"
        elif table == "achievements":
            sq = "select name from acnievements where id = :id"
        self.cursor.execute(sq, {"id" : ID})
        return self.cursor.fetchone()

    def getList(self, table: str, condition: str) -> list:
        sq = f"select * from {table} "
        if condition:
            sq += condition
        self.cursor.execute(sq)
        return self.cursor.fetchall()

    def getByField(self, table: str, **fields: str) -> tuple:
        if len(fields) > 1:
            return None
        sq = f"select * from {table} where {fields[0]}=:{fields[0]}"
        self.cursor.execute(sq, fields)
        return self.cursor.fetchone()

    def getMarksTable(self, group, with_group, works_number):
        sq = "select first_name, second_name, middle_name"
        if with_group:
            sq += ", year, number, subgroup"
        for i in range(1, works_number+1):
            sq += f", m{i}.mark, m{i}.pass_date, d{i}.deadline"
        sq += " from students "
        if with_group:
            sq += "inner join groups on groups.id = students.group_id "
        for i in range(1, works_number+1):
            sq += f"left join marks m{i} on m{i}.student_id = students.id and m{i}.work_id = {i} "
            sq += f"left join deadlines d{i} on d{i}.group_id = students.group_id and d{i}.work_id = {i} "
        if not with_group:
            year = int(group[2:4])+2000
            number = int(group[5:7])
            subgroup = int(group[8])
            sq += f"where students.group_id = {self.getID('groups', year=year, number=number, subgroup=subgroup)} "
        sq += "order by first_name, second_name, middle_name "
        self.cursor.execute(sq)
        return self.cursor.fetchall()

    def insertRecord(self, table: str, data: list) -> bool:
        pass

    def updateRecord(self, table: str, **fields: str) -> bool:
        pass

    def deleteRecord(self, table: str, ID: int) -> bool:
        pass

    def close(self):
        self.conn.close()
