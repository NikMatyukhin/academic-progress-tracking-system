from LocalService import LocalService

from RemoteService import RemoteService

from DatabaseWrapperInterface import DatabaseWrapperInterface


class DatabaseController(DatabaseWrapperInterface):
    def __init__(self, lservice: LocalService, rservice: RemoteService):
        self.localService = lservice
        self.remoteService = rservice

    def getID(self, table: str, **fields: str) -> int:
        pass

    def getName(self, table: str, ID: int) -> str:
        pass

    def getList(self, table: str) -> list:
        pass

    def getByField(self, table: str, field: str) -> tuple:
        pass

    def insertRecord(self, table: str, data: list) -> bool:
        pass

    def updateRecord(self, table: str, data: list) -> bool:
        pass

    def deleteRecord(self, table: str, fieldName: str, field: str) -> bool:
        pass

    def close():
        pass

    def checkConnection() -> bool:
        pass

    def syncronize() -> bool:
        pass
