import requests

from DatabaseWrapperInterface import DatabaseWrapperInterface


class RemoteService(QDatabaseWrapperInterface):
    def __init__(self, url):
        self.url = url

        response = requests.post()
        response = requests.get()

        response.json()

    def getID(self, table: str, **fields: str) -> int:
        pass

    def getName(self, table: str, ID: int) -> str:
        pass

    def getList(self, table: str) -> list:
        pass

    def getByField(self, table: str, field: str) -> tuple:
        pass

    def insertRecord(self, table: str, data: list) -> bool:
        pass

    def updateRecord(self, table: str, data: list) -> bool:
        pass

    def deleteRecord(self, table: str, fieldName: str, field: str) -> bool:
        pass

    def close():
        pass
