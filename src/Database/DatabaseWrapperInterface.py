import abc


class DatabaseWrapperInterface:
    @classmethod
    def __subclasshook__(cls, subclass):
        return (hasattr(subclass, 'getID') and callable(subclass.getID) and
                hasattr(subclass, 'getName') and callable(subclass.getName) and
                hasattr(subclass, 'getList') and callable(subclass.getList) and
                hasattr(subclass, 'getByField') and callable(subclass.getByField) and
                hasattr(subclass, 'insertRecord') and callable(subclass.insertRecord) and
                hasattr(subclass, 'updateRecord') and callable(subclass.updateRecord) and
                hasattr(subclass, 'deleteRecord') and callable(subclass.deleteRecord) and
                hasattr(subclass, 'close') and callable(subclass.close) or
                NotImplemented)

    @abc.abstractmethod
    def getID(self, table: str, **fields: str):
        raise NotImplementedError

    @abc.abstractmethod
    def getName(self, table: str, ID: int):
        raise NotImplementedError

    @abc.abstractmethod
    def getList(self, table: str):
        raise NotImplementedError

    @abc.abstractmethod
    def getByField(self, table: str, field: str):
        raise NotImplementedError

    @abc.abstractmethod
    def insertRecord(self, table: str, data: list):
        raise NotImplementedError

    @abc.abstractmethod
    def updateRecord(self, table: str, data: list):
        raise NotImplementedError

    @abc.abstractmethod
    def deleteRecord(self, table: str, fieldName: str, field: str):
        raise NotImplementedError

    @abc.abstractmethod
    def close():
        raise NotImplementedError