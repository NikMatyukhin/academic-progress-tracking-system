import sys
import time

from unittest import TestCase

from Analytic.MarksAnalyzer import MarksAnalyzer
from Widgets.QAnalyticDialog import QAnalyticDialog

from PySide2.QtWidgets import QApplication, QListView
from PySide2.QtTest import QTest
from PySide2.QtCore import Qt, QPoint


app = QApplication(sys.argv)


class AnalyticDialogTests(TestCase):

    def setUp(self):
        self.form = QAnalyticDialog()
        self.group_list = ["Все группы","КИ20-16/1Б","КИ20-16/2Б","КИ20-17/1Б","КИ20-17/2Б"]

    def test_defaults(self):
        self.assertEqual(self.form.ui.comboBox.currentText(), "Все группы")

    def test_group_choose(self):
        group_combo_box = self.form.ui.comboBox
        recount_button = self.form.ui.pushButton

        QTest.mouseClick(group_combo_box, Qt.LeftButton)
        time.sleep(1)

        lw = group_combo_box.findChild(QListView)

        print()

        for r in range(lw.model().rowCount()):
            index = lw.model().index(r,0)
            itemPt = lw.visualRect(index).center()
            name = lw.model().index(r,0).data(Qt.DisplayRole)
            print(f'click on [{name}] button with [{itemPt}] center')

            time.sleep(1)
            QTest.mouseClick(lw.viewport(), Qt.LeftButton, pos = itemPt, delay = 0)

            self.form.currentGroupChanged(name)

            QTest.mouseClick(recount_button, Qt.LeftButton)
            self.form.currentGroupChanged(name)

            self.assertEqual(self.form.group, self.group_list[r])

            time.sleep(1)
            QTest.mouseClick(group_combo_box, Qt.LeftButton)

        QTest.keyClick(group_combo_box, Qt.Key_Escape)

    def test_load_data(self):
        self.form.ui.comboBox.setCurrentIndex(3)

        button = self.form.ui.pushButton
        QTest.mouseClick(button, Qt.LeftButton)
        self.assertEqual(len(self.form.data), 11)

        self.form.ui.comboBox.setCurrentIndex(4)

        button = self.form.ui.pushButton
        QTest.mouseClick(button, Qt.LeftButton)
        self.assertEqual(len(self.form.data), 8)

    def tearDown(self):
        pass

if __name__ == "__main__":
    unittest.run()